# APPUNTI GIT [by Stefano Goffi]

## Indice
#### 1. [Introduzione](#introduzione)
#### 2. [Comandi](#comandi)
#### 3. [Branch](#branch)
#### 4. [Remote](#remote)
#### 5. [Riferimenti](#riferimenti)
***
## Introduzione

Come per molte grandi cose della vita, Git e' nato con un po' di distruzione creativa e polemiche infuocate.

Fin dalla sua nascita nel **2005** Git si e' evoluto e maturato per essere facile da usare e tuttora mantiene le sue qualita' iniziali. E' incredibilmente veloce, e' molto **efficiente con progetti grandi** e ha un incredibile **sistema di ramificazioni**, per lo *sviluppo non lineare*

Per scaricare e installare Git:
- Linux: https://git-scm.com/download/linux
- macOS: https://git-scm.com/download/mac
- Windows: https://git-scm.com/download/win
***
## Comandi
una volta scaricato git bisogna eseguire questi 2 comandi per la creazione di un utente (configurazione nickname e email)
```
// creazione di un utente (nome utente)
$ git config --global user.name "stefano"

// creazione di un utente (email)
$ git config --global user.email "stefanoEsempio@esempio.it"
```
Se dovessi avere bisogno di aiuto durante l'uso di Git:
```
// non ce niente di male a chiedere aiuto
$ git help
```
Una volta creato l'utente sara' possibile creare la nostra prima repository, tramite il comando sottostante:

- ### Inizializzazione di un repository in una directory esistente
Mi sembra al quanto scontanto dire che se devi crare una repository devi navigare dentro una **directory** gia' esistente sul tuo dispositivo, successivamente bisogna digitare il seguente comando:

```
// inizializzazione di una repository
$ git init
```
Questo crea una nuova sottodirectory denominata ***.git*** che contiene tutti i file di repository necessari: **uno scheletro di repository Git.**

Se vuoi iniziare dovresti probabilmente tenere traccia di quei file ed eseguire un commit iniziale. Puoi farlo con alcuni comandi ***git add*** che specificano i file che vuoi tracciare, seguiti da un commit git:
```
// aggiungi al commit tutti i file della directory
$ git add * 

// aggiungi al commit il file selezionato
$ git add LICENZA

// commit veloce, non apre editor testo (solo messaggi brevi)
$ git commit -m 'versione iniziale del progetto'
```
---
### Altri comandi

- Visualizzare lo status della directory
```
$ git status
```
- Visualizzare elementi della direcotry
```
$ git show
```
- Mostra tutto l'elenco dei commit effettuati in quel branch
```
$ git log
```
***
## Branch
In Git, un branch e' un puntatore ad un commit specifico nella storia del repository. In altre parole, un branch rappresenta una linea di sviluppo indipendente dal branch principale (detto "master" o "main").

Il vantaggio principale di utilizzare i branch in Git e' che permettono di lavorare su una nuova feature o correzione di bug in modo isolato rispetto al codice principale, senza rischiare di causare problemi al resto del progetto. Ogni branch puo' essere sviluppato e testato separatamente, e poi unito al branch principale solo quando la nuova funzionalita' o correzione e' pronta.
### Comandi
- Mostra tutti i branch nel repository
```
$ git branch
```
- Crea un nuovo branch
```
$ git branch NuovoBranch
```
- Passa al branch specifico
```
$ git checkout NuovoBranch
```
- Unisce il branch specificato con il branch attuale
```
$ git merge NuovoBranch
```
***
## Remote
Un repository remoto puo' essere usato come una copia di backup del repository locale, ma l'uso principale e' quello di collaborare con altri sviluppatori. I repository remoti consentono a diversi sviluppatori di lavorare sugli stessi file, coordinandosi tra loro e condividendo le modifiche apportate.

Per utilizzare un repository remoto, e' necessario creare una connessione tra il repository locale e il repository remoto. Questo viene fatto aggiungendo un "remote" al repository locale. Un "remote" e' essenzialmente un alias per l'URL del repository remoto.


- Aggiunge un nuovo remote al repository
```
$ git remote add 
```
- Mostra tutti i remote
```
$ git remote -v
```
- Carica le modifiche in un remote
```
$ git push
```
- Scarica le modifiche da un remote
```
$ git pull
```
- Scarica i dati dal remote senza unire i branch
```
$ git fetch
```
***
## Riferimenti

Durante la stesura di questo cheatsheet ho preso riferimeto da questi siti qui

- *Git*: https://git-scm.com
- *IONOS Digital Guide*: https://www.ionos.it/digitalguide/siti-web/programmazione-del-sito-web/comandi-git/
- *w3school*: https://www.w3schools.com/git/

menzione d'onore a un LearnGame interamente su Git, perfetto per farsi un idea sia concettuale che grafica di branching e merging

***LaernGitBranch*** https://learngitbranching.js.org/?locale=it_IT
***